FROM openjdk:8-alpine
COPY target/config-server*.jar config-server.jar
VOLUME /root/projects/test-cloud/configs
CMD ["java", "-jar", "config-server.jar"]
